#!/usr/bin/env python3
import argparse
import boto3
import json
from datetime import datetime
from tabulate import tabulate

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)

def get_ec2_from_region(region_name=None,filter=None):
    connect = boto3.Session(region_name=region_name)
    ec2 = connect.client('ec2')
    response = ec2.describe_instances(Filters=[{'Name': 'tag:Name', 'Values': [filter]}])
    
    instances = []
    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            
            instance_data = {
                'InstanceName': instance.get('Tags', [{'Key': 'Name', 'Value': 'N/A'}])[0].get('Value'),  # Instance Name (default to 'N/A' if no name tag)
                'InstanceId': instance['InstanceId'],
                'InstanceType': instance['InstanceType'],
                'State': instance['State']['Name'],
                'PrivateIpAddress': instance.get('PrivateIpAddress', 'N/A'),
                'PublicIpAddress': instance.get('PublicIpAddress', 'N/A'),
            }
            
            # Get attached EBS volumes information
            volumes_info = ec2.describe_volumes(Filters=[{'Name': 'attachment.instance-id', 'Values': [instance['InstanceId']]}])
            total_size_ebs_volumes = sum([volume['Size'] for volume in volumes_info['Volumes']])
            
            instance_data['total-size-ebs-volumes'] = total_size_ebs_volumes
            
            instances.append(instance_data)
    
    return instances

def lambda_handler(event=None, context=None):

    parser = argparse.ArgumentParser(description="Boto3Function")
    parser.add_argument("-filter", help="Value for Filtering by tag name", default='*')
    parser.add_argument("-region", help="Value for selecting the region", default="eu-west-1")
    parser.add_argument("-sort", help="Value for Sorting the table", choices=["asc","desc"], default="desc")
    args = parser.parse_args()
    
    get = get_ec2_from_region(args.region,args.filter)
    
    # Sort instances based on total-size-ebs-volumes
    if args.sort == 'asc':
        get.sort(key=lambda x: x.get('total-size-ebs-volumes', 0))
    elif args.sort == 'desc':
        get.sort(key=lambda x: x.get('total-size-ebs-volumes', 0), reverse=True)
    
    # Convert JSON data to a list of lists for tabulate
    table_data = [[
        item.get('InstanceName', ''),
        item.get('InstanceId', ''),
        item.get('InstanceType', ''),
        item.get('State', ''),
        item.get('PrivateIpAddress', ''),
        item.get('PublicIpAddress', ''),
        item.get('total-size-ebs-volumes', '')
    ] for item in get]

    # Calculate total ebs volumes
    total_ebs_volumes = sum(item.get('total-size-ebs-volumes', 0) for item in get)
    
    # Define table headers
    headers = [
        "InstanceName",
        "InstanceId",
        "InstanceType",
        "State",
        "PrivateIpAddress",
        "PublicIpAddress",
        "total-size-ebs-volumes"
    ]
    
    # Append an empty row for spacing
    table_data.append([""] * len(headers))
    
    # Add a row with total ebs volumes to the table data
    total_row = ["", "", "", "", "Total_Size_EBS_Volumes", "", total_ebs_volumes]
    table_data.append(total_row)

    table = tabulate(table_data, headers=headers, tablefmt="grid")
    
    # Print the table
    print(table)

    return {
        'statusCode': 200,
        'body': json.dumps('Done')
    }

if __name__ == "__main__":
    lambda_handler()
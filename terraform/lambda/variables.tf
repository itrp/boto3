variable "region" {
  type = string
  default = "eu-west-1"
}
variable "runtime" {
  type = string
  default = "python3.11"
}
variable "architectures" {
  type = list(any)
  default = ["x86_64"]
}
variable "filename" {
  type = string
  default = "lambda_function.zip"
}
variable "function_name" {
  type = string
  default = "PythonLambda"

}
variable "handler" {
  type = string
  default = "lambda_function.lambda_handler"
}
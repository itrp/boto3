resource "aws_lambda_function" "python_lambda" {
  filename      = data.archive_file.lambda_zip.output_path
  function_name = var.function_name
  role          = aws_iam_role.lambda_exec.arn
  handler       = var.handler
  runtime       = var.runtime
  architectures = var.architectures
  layers        = [aws_lambda_layer_version.this.arn]
  timeout = "60"
  source_code_hash = filebase64sha256(data.archive_file.lambda_zip.output_path)
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file  = "${path.module}/src/lambda_function.py"
  output_path = "${path.module}/src/lambda_function.zip"
}

resource "null_resource" "delete_zip" {

  provisioner "local-exec" {
    command = "rm -f ./src/lambda_function.zip"
  }
  depends_on = [ aws_lambda_function.python_lambda ]
}


resource "aws_iam_role" "lambda_exec" {
  name = "lambda_execution_role"
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    aws_iam_policy.lambda_ec2_policy.arn
    ]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action    = "sts:AssumeRole"
      },
    ]
  })

  tags = {
    Name = "LambdaExecutionRole"
  }
}

resource "aws_iam_policy" "lambda_ec2_policy" {
  name        = "lambda_ec2_policy"
  description = "Policy to allow Lambda to describe EC2 instances"
  policy      = file("./src/ec2_permission.json")
}

resource "aws_iam_role_policy_attachment" "lambda_exec_attach" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = aws_iam_policy.lambda_ec2_policy.arn
}

resource "aws_lambda_layer_version" "this" {
  filename   = "${path.module}/src/tabulate_layer.zip"
  layer_name = "TabulateLayer"

  compatible_runtimes = ["python3.8","python3.9","python3.10","python3.11","python3.12"]
}

resource "aws_lambda_invocation" "this" {
  function_name = aws_lambda_function.python_lambda.function_name

  input = jsonencode({
    region = "eu-west-1"
    sort = "asc"
    filter = "Server*"
  })
}

output "result_entry" {
  value = jsondecode(aws_lambda_invocation.this.result)
}
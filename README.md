# EC2 Instance Info Script

## Description
This Python script uses the Boto3 library to retrieve information about EC2 instances in an AWS region. It can filter instances based on tag names, sort them by EBS volume size, and display the information in a tabular format using the Tabulate library.

## Prerequisites
1. Python 3.x installed on your system.
2. Pip package manager for Python (pip3 for Python 3.x).
3. AWS CLI configured with appropriate access credentials.
4. Git installed (optional, for cloning the repository).

## Installation
1. Clone the repository:
    ```bash
    git clone -i ~/.ssh/gitlab_rsa git@gitlab.com:itrp/boto3.git boto3_gitlab/
    ```

2. Change into the project directory:
    ```bash
    cd boto3_gitlab/
    ```

3. Install the required Python packages:
    ```bash
    pip3 install -r requirements.txt
    ```

## Usage
1. Navigate to the project directory:
    ```bash
    cd boto3_gitlab/
    ```

2. Run the script using the following command:
    ```bash
    python3 lambda_function.py -filter "Server*" -sort desc
    ```

## Parameter Usage
- `-filter "Server*"`: Specify a case-sensitive filter pattern for EC2 instance names. Use '*' as a wildcard character. By default it brings all instances.
- `-sort asc/desc`: Sort instances based on total-size-ebs-volumes in ascending (`asc`) or descending (`desc`) order.
- `-region eu-west-1`: Script uses eu-west-1 as default region.

## Script Details
- `lambda_function.py`: Main script file.
- `requirements.txt`: Contains the required Python packages and their versions.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
